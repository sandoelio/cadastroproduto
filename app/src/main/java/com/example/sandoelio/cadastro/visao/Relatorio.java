package com.example.sandoelio.cadastro.visao;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sandoelio.cadastro.R;
import com.example.sandoelio.cadastro.modelos.Cliente;
import com.example.sandoelio.cadastro.modelos.Produto;
import com.example.sandoelio.cadastro.modelos.Venda;

public class Relatorio extends AppCompatActivity {

    private TextView relatorioId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio);

        relatorioId = (TextView) findViewById(R.id.relatorioId);

        Cliente cliente = (Cliente) getIntent().getExtras().get("cliente");
        if (!is_null(cliente))
            CadastroCliente();
        Produto produto = (Produto) getIntent().getExtras().get("produto");
        if (!is_null(produto))
            CadastroProduto();
        Venda venda = (Venda) getIntent().getExtras().get("venda");
        if (!is_null(venda))
            CadastroVenda();
    }
    /*----------------------------------------------------------------------------*/
    //metodo de verificação
    private boolean is_null(Object obj) {
        return obj == null;
    }

    /*-----------------------------------------------------------------------------*/
    /*Metodo cadastroCliente*/
   public void CadastroCliente() {

        Cliente cliente = (Cliente) getIntent().getExtras().get("cliente");

            relatorioId.setText("CODIGO DO CLIENTE : " + cliente.getCliente_cod() + "\n"
                                + "NOME DO CLIENTE   : " + cliente.getCliente_nome() + "\n"
                                + "ENDEREÇO DO CLIENTE : " + cliente.getCliente_endereco() + "\n"
                                + "TELEFONE DO CLIENTE : " + cliente.getCliente_telefone());
    }
    /*------------------------------------------------------------------------------------ */
    //Metodo cadastroProduto
     public void CadastroProduto() {

         Produto produto = (Produto) getIntent().getExtras().get("produto");

         relatorioId.setText("CODIGO DO PRODUTO : " + produto.getCodigo_produto() + "\n"
                             + "NOME DO PRODUTO   : " + produto.getNome_produto() + "\n"
                             + "FORNECEDOR : " + produto.getFornecedor() + "\n");
     }

    /*------------------------------------------------------------------------------------ */
    //Metodo vendas
    public void CadastroVenda() {

        Venda venda = (Venda) getIntent().getExtras().get("venda");

        relatorioId.setText("CODIGO DO CLIENTE : " + venda.getCodigo_cliente_venda() + "\n"
                + "CODIGO DO PRODUTO   : " + venda.getCodigo_produto_venda() + "\n"
                + "VALOR DA VENDA : " + venda.getValor_venda() + "\n"
                + "CODIGO DO PAGAMENTO : " + venda.getCodigo_pagamento_venda() + "\n"
                + "VALOR PAGO : " + venda.getValor_pago_venda() + "\n" );

    }
}
