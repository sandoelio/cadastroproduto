package com.example.sandoelio.cadastro.modelos;

import java.io.Serializable;

public class Venda implements Serializable{

    private Integer codigo_cliente_venda;
    private Integer codigo_produto_venda;
    private Double valor_venda;
    private Integer codigo_pagamento_venda;
    private Double valor_pago_venda;


    public Venda(Integer codigo_cliente_venda, Integer codigo_produto_venda, Double valor_venda,
                 Integer codigo_pagamento_venda, Double valor_pago_venda) {
        this.codigo_cliente_venda = codigo_cliente_venda;
        this.codigo_produto_venda = codigo_produto_venda;
        this.valor_venda = valor_venda;
        this.codigo_pagamento_venda = codigo_pagamento_venda;
        this.valor_pago_venda = valor_pago_venda;
    }

    public Venda() {
    }

    public Integer getCodigo_cliente_venda() {
        return codigo_cliente_venda;
    }

    public void setCodigo_cliente_venda(Integer codigo_cliente_venda) {
        this.codigo_cliente_venda = codigo_cliente_venda;
    }

    public Integer getCodigo_produto_venda() {
        return codigo_produto_venda;
    }

    public void setCodigo_produto_venda(Integer codigo_produto_venda) {
        this.codigo_produto_venda = codigo_produto_venda;
    }

    public Double getValor_venda() {
        return valor_venda;
    }

    public void setValor_venda(Double valor_venda) {
        this.valor_venda = valor_venda;
    }

    public Integer getCodigo_pagamento_venda() {
        return codigo_pagamento_venda;
    }

    public void setCodigo_pagamento_venda(Integer codigo_pagamento_venda) {
        this.codigo_pagamento_venda = codigo_pagamento_venda;
    }

    public Double getValor_pago_venda() {
        return valor_pago_venda;
    }

    public void setValor_pago_venda(Double valor_pago_venda) {
        this.valor_pago_venda = valor_pago_venda;
    }
}
