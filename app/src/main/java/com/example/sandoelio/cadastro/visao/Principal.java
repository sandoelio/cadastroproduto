package com.example.sandoelio.cadastro.visao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.example.sandoelio.cadastro.R;

public class Principal extends AppCompatActivity {

    private Button btnCadCli;
    private Button btnCadVend;
    private Button btnCadProd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        btnCadCli  = (Button) findViewById(R.id.btnCadCli);
        btnCadVend = (Button) findViewById(R.id.btnCadVend);
        btnCadProd = (Button) findViewById(R.id.btnCadProd);

        /*---------------------------------------------------*/
        //Metodo de click do botao cadastro cliente
        btnCadCli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Principal.this,CadastroCliente.class);
                startActivity(intent);
            }
        });
        /*---------------------------------------------------*/
        //Metodo de click do botao cadastro vendas
        btnCadVend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Principal.this,CadastroVendas.class);
                startActivity(intent);
            }
        });
        /*---------------------------------------------------*/
        //Metodo de click do botao cadastro produtos
        btnCadProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Principal.this,CadastroProduto.class);
                startActivity(intent);
            }
        });
    }
}
