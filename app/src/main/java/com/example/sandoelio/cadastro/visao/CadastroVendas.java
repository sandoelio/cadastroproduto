package com.example.sandoelio.cadastro.visao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sandoelio.cadastro.R;
import com.example.sandoelio.cadastro.modelos.Venda;

public class CadastroVendas extends AppCompatActivity {

    private EditText vendas_cod_cliente;
    private EditText vendas_cod_produto;
    private EditText vendas_val_venda;
    private EditText vendas_cod_pagamento;
    private EditText vendas_val_pago;
    private Button vender;

    private Integer vendaCodigoCliente;
    private Integer vendaCodigoPrduto;
    private Double vendaValorVenda;
    private Integer vendaCodigoPagamento;
    private Double vendaValorPago;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_vendas);

        vendas_cod_cliente = findViewById(R.id.vendas_cod_cliente);
        vendas_cod_produto = findViewById(R.id.vendas_cod_produto);
        vendas_val_venda   = findViewById(R.id.vendas_val_venda);
        vendas_cod_pagamento = findViewById(R.id.vendas_cod_pagamento);
        vendas_val_pago = findViewById(R.id.vendas_val_pago);
        vender = findViewById(R.id.btn_vendas_vender);

        /*--------------------------------------------------------------------*/
        //CLICK DO BOTAO VENDER

        vender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vendaCodigoCliente = Integer.parseInt(vendas_cod_cliente.getText().toString());
                vendaCodigoPrduto = Integer.parseInt(vendas_cod_produto.getText().toString());
                vendaValorVenda = Double.parseDouble(vendas_val_venda.getText().toString());
                vendaCodigoPagamento = Integer.parseInt(vendas_cod_pagamento.getText().toString());
                vendaValorPago = Double.parseDouble(vendas_val_pago.getText().toString());

                Intent intentVenda = new Intent(CadastroVendas.this,Relatorio.class);

                Venda venda = new Venda();

                venda.setCodigo_cliente_venda(vendaCodigoCliente);
                venda.setCodigo_produto_venda(vendaCodigoPrduto);
                venda.setValor_venda(vendaValorVenda);
                venda.setCodigo_pagamento_venda(vendaCodigoPagamento);
                venda.setValor_pago_venda(vendaValorPago);

                intentVenda.putExtra("venda",venda);
                startActivity(intentVenda);
                finish();
            }
        });
    }
}
