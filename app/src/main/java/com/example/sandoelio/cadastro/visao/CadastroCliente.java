package com.example.sandoelio.cadastro.visao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sandoelio.cadastro.R;
import com.example.sandoelio.cadastro.modelos.Cliente;

public class CadastroCliente extends AppCompatActivity {

    private EditText cliente_cod;
    private EditText cliente_nome;
    private EditText cliente_endereco;
    private EditText cliente_tel;
    private Button btn_cliente_salvar;

    private Integer codigo;
    private String nome;
    private String endereco;
    private Integer telefone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_cliente);

        cliente_cod        = (EditText) findViewById(R.id.cliente_cod);
        cliente_nome       = (EditText)findViewById(R.id.cliente_nome);
        cliente_endereco   = (EditText)findViewById(R.id.cliente_endereco);
        cliente_tel        = (EditText)findViewById(R.id.cliente_tel);
        btn_cliente_salvar = (Button) findViewById(R.id.btn_cliente_salvar);


        /*--------------------------------------------------------------------*/
        //CLICK DO BOTAO SALVAR

        btn_cliente_salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                codigo   = Integer.parseInt(cliente_cod.getText().toString());
                nome     = cliente_nome.getText().toString();
                endereco = cliente_endereco.getText().toString();
                telefone = Integer.parseInt(cliente_tel.getText().toString());

                Intent intentCliente = new Intent(CadastroCliente.this,Relatorio.class);

                Cliente cliente = new Cliente();

                cliente.setCliente_cod(codigo);
                cliente.setCliente_nome(nome);
                cliente.setCliente_endereco(endereco);
                cliente.setCliente_telefone(telefone);

                intentCliente.putExtra("cliente",cliente);
                startActivity(intentCliente);
                finish();

            }
        });

    }
}
