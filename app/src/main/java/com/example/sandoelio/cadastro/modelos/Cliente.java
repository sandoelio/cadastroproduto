package com.example.sandoelio.cadastro.modelos;

import java.io.Serializable;

public class Cliente implements Serializable{

    private Integer cliente_cod;
    private String cliente_nome;
    private String cliente_endereco;
    private Integer cliente_telefone;


    public Cliente(Integer cliente_cod, String cliente_nome, String cliente_endereco, Integer cliente_telefone) {
        this.cliente_cod = cliente_cod;
        this.cliente_nome = cliente_nome;
        this.cliente_endereco = cliente_endereco;
        this.cliente_telefone = cliente_telefone;
    }

    public Cliente() {

    }


    public Integer getCliente_cod() {
        return cliente_cod;
    }

    public void setCliente_cod(Integer cliente_cod) {
        this.cliente_cod = cliente_cod;
    }

    public String getCliente_nome() {
        return cliente_nome;
    }

    public void setCliente_nome(String cliente_nome) {
        this.cliente_nome = cliente_nome;
    }

    public String getCliente_endereco() {
        return cliente_endereco;
    }

    public void setCliente_endereco(String cliente_endereco) {
        this.cliente_endereco = cliente_endereco;
    }

    public Integer getCliente_telefone() {
        return cliente_telefone;
    }

    public void setCliente_telefone(Integer cliente_telefone) {
        this.cliente_telefone = cliente_telefone;
    }

}
