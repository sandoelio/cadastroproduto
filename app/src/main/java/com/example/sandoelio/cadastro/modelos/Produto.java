package com.example.sandoelio.cadastro.modelos;

import java.io.Serializable;

public class Produto implements Serializable{

    private Integer codigo_produto;
    private String nome_produto;
    private String fornecedor;

    public Produto(Integer codigo_produto, String nome_produto, String fornecedor, Integer controleProd) {
        this.codigo_produto = codigo_produto;
        this.nome_produto = nome_produto;
        this.fornecedor = fornecedor;
    }

    public Produto() {

    }

    public Integer getCodigo_produto() {
        return codigo_produto;
    }

    public void setCodigo_produto(Integer codigo_produto) {
        this.codigo_produto = codigo_produto;
    }

    public String getNome_produto() {
        return nome_produto;
    }

    public void setNome_produto(String nome_produto) {
        this.nome_produto = nome_produto;
    }

    public String getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

}
