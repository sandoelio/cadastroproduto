package com.example.sandoelio.cadastro.visao;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.sandoelio.cadastro.R;
import com.example.sandoelio.cadastro.modelos.Produto;

public class CadastroProduto extends AppCompatActivity {
    private EditText codigo_produto;
    private EditText nome_produto;
    private EditText fornecedor;
    private Button salvar;

    private Integer codigoProd;
    private String nomeProd;
    private String fornecedorProd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_produto);

        codigo_produto = findViewById(R.id.produto_codigo);
        nome_produto = findViewById(R.id.produto_nome);
        fornecedor = findViewById(R.id.produto_fornecedor);
        salvar = findViewById(R.id.btn_produto_salvar);

        /*--------------------------------------------------------------------*/
        //CLICK DO BOTAO SALVAR

        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codigoProd     = Integer.parseInt(codigo_produto.getText().toString());
                nomeProd       = nome_produto.getText().toString();
                fornecedorProd = fornecedor.getText().toString();

                Intent intentPrduto = new Intent(CadastroProduto.this,Relatorio.class);
                Produto produto = new Produto();

                produto.setCodigo_produto(codigoProd);
                produto.setNome_produto(nomeProd);
                produto.setFornecedor(fornecedorProd);

                intentPrduto.putExtra("produto",produto);
                startActivity(intentPrduto);
                finish();
            }
        });
    }
}
